import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import './index.css';
import Dashboard from './components/Dashboard';
import Login from './components/Login/login'
import Preferences from './components/Preferences';

function Home() {
  const [loginToken, setloginToken] = useState();

  // if user is not logged in
  if(!loginToken) {
    return (
      <Login setloginToken={setloginToken} />
    );
  }

  return(
    <div className="home">
      <h1>Planner</h1>
      <BrowserRouter>
        <Switch>
          <Route path="/dashboard">
            <Dashboard />
          </Route>
          <Route path="/preferences">
            <Preferences />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default Home;