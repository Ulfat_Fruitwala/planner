import './login.css'
import { AiOutlineUser, AiOutlineLock } from 'react-icons/ai'


export default function Login(){
  return(
    <div className="login-page">
      <div className="box">
        <h1>Sign in to continue</h1>
        <div>
          <span className="input-div"> 
            <AiOutlineUser className="icon"/>
            <input className="input" type="text" placeholder= "Username"/>
          </span>
        </div>
        <div>
          <span className="input-div">
            <AiOutlineLock className="icon" />
            <input className="input" type="password" placeholder="Password" />
          </span>
        </div>
        <button className="login-button">Sign In</button>
        </div>
    </div>
  )
}